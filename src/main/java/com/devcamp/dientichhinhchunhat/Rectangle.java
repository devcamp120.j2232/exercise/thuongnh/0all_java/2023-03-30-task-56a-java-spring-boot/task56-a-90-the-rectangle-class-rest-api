package com.devcamp.dientichhinhchunhat;



public class Rectangle {
    // khai báo thuộc tính 
    private float length = 1f;
    private float width = 1f;
    // khai báo phương thức  khởi tạo . method 
    public Rectangle() {
    }
    // khai báo phương thức khởi tạo hai thuộc tính 
    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }
    // phương thức trả về  giá trị chiều dài của đối tượng 
    public float getLength() {
        return length;
    }
    // phương thức cài đặt chiều dài  . thay đổi chiều dài cho đối tượng 
    public void setLength(float length) {
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }
    // phương thức trả về giá trị là diện tích hình chữ nhật 
    public double getArena(){
        return this.width * this.length;
    }

    // phương thúc trả về giá trị chu vi của đối tượng ( hình chữ nhật )
    public double getPerimeter(){
        return (this.width + this.length) * 2;
    }
    // in ra màn hình   
    public String toString(){
        return "Rectangle [ length = " + this.length + ", width = " + this.width + "]";
    }   

    




    
}

