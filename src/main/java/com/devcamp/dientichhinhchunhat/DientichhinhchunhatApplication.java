package com.devcamp.dientichhinhchunhat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DientichhinhchunhatApplication {

	public static void main(String[] args) {
		SpringApplication.run(DientichhinhchunhatApplication.class, args);
	}

}
