package com.devcamp.dientichhinhchunhat;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class getApiRectangle {

    // diện tích hình chữ nhật 
    @GetMapping("/rectangle-area")
    public double getArea(@RequestParam(value = "width", defaultValue = "0") float width,
            @RequestParam(value = "length", defaultValue = "0") float length) {
        // khởi tạo đối tượng
        Rectangle hinhchunhat = new Rectangle(length, width);
        return hinhchunhat.getArena();

    }

    // chu vi hình chữ nhật 
    @GetMapping("/rectangle-perimeter")
    public double getPerimeter(@RequestParam(value = "width", defaultValue = "0") float width,
            @RequestParam(value = "length", defaultValue = "0") float length) {
        // khởi tạo đối tượng
        Rectangle hinhchunhat = new Rectangle(length, width);
        return hinhchunhat.getPerimeter();

    }
}
